using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Onestop.Navigation.Models;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Handlers;
using Orchard.ContentManagement.Records;
using Orchard.Data;
using Orchard.Logging;

namespace Onestop.Navigation.Services {
    [UsedImplicitly]
    public class DefaultVersionManager : IVersionManager {
        private readonly IContentManager _content;
        private readonly IRepository<ContentItemVersionRecord> _contentItemVersionRepository;
        private readonly IRepository<ContentItemRecord> _contentItemRepository;
        private readonly IRepository<VersionInfoPartRecord> _infoPartRepository;
        private readonly Lazy<IEnumerable<IContentHandler>> _handlers;
        private readonly IDictionary<int, ContentItem> _cache;

        public DefaultVersionManager(
            IContentManager content,
            IRepository<ContentItemVersionRecord> contentItemVersionRepository,
            IRepository<ContentItemRecord> contentItemRepository,
            Lazy<IEnumerable<IContentHandler>> handlers,
            IRepository<VersionInfoPartRecord> infoPartRepository) {
            _content = content;
            _contentItemVersionRepository = contentItemVersionRepository;
            _contentItemRepository = contentItemRepository;
            _handlers = handlers;
            _infoPartRepository = infoPartRepository;
            Logger = NullLogger.Instance;
            _cache = new Dictionary<int, ContentItem>();
        }

        public ILogger Logger { get; set; }

        public IEnumerable<IContentHandler> Handlers {
            get { return _handlers.Value; }
        }

        /// <summary>
        /// Creates new version based on provided content item.
        /// </summary>
        /// <param name="existingContentItem">Item version to base new version on.</param>
        /// <returns>Created version.</returns>
        public ContentItem NewVersion(ContentItem existingContentItem) {
            return NewVersion(new[] {existingContentItem}).FirstOrDefault();
        }

        public IEnumerable<ContentItem> NewVersion(params ContentItem[] existingContentItems)
        {
            var ids = existingContentItems.Select(r => r.Id).ToArray();
            var latestVersions = _contentItemVersionRepository.Table
                .Where(r => ids.Contains(r.ContentItemRecord.Id) && r.Latest)
                .OrderByDescending(r => r.Id)
                .ToLookup(r => r.ContentItemRecord.Id);

            var nextNumbers = _contentItemVersionRepository
                .Table
                .Where(r => ids.Contains(r.ContentItemRecord.Id))
                .GroupBy(r => r.ContentItemRecord.Id)
                .Select(g => new {g.Key, Value = g.Max(r => (int?) r.Number)})
                .ToDictionary(g => g.Key, g => (g.Value ?? 0) + 1);

            var buildingItemVersionRecords = new Dictionary<int, ContentItemVersionRecord>();

            foreach (var existingContentItem in existingContentItems)
            {
                // locate the existing and the current latest versions, allocate building version
                var contentItemRecord = existingContentItem.Record;
                var existingItemVersionRecord = existingContentItem.VersionRecord;
                var buildingItemVersionRecord = new ContentItemVersionRecord
                {
                    ContentItemRecord = contentItemRecord,
                    Latest = true,
                    Published = false,
                    Data = existingItemVersionRecord.Data,

                    // Always set up version number to max from existing to avoid conflicts
                    Number = nextNumbers.ContainsKey(contentItemRecord.Id) ? nextNumbers[contentItemRecord.Id] : 1
                };

                if (latestVersions.Contains(contentItemRecord.Id))
                {
                    foreach(var latestVersion in latestVersions[contentItemRecord.Id])
                    {
                        latestVersion.Latest = false;
                    }
                }

                buildingItemVersionRecords[contentItemRecord.Id] = buildingItemVersionRecord;
                _contentItemVersionRepository.Create(buildingItemVersionRecord);
            }

            _contentItemVersionRepository.Flush();

            foreach (var existingContentItem in existingContentItems)
            {
                var contentItemRecord = existingContentItem.Record;
                if (!buildingItemVersionRecords.ContainsKey(contentItemRecord.Id)) continue;

                var buildingItemVersionRecord = buildingItemVersionRecords[contentItemRecord.Id];
                var existingItemVersionRecord = existingContentItem.VersionRecord;

                var buildingContentItem = _content.New(existingContentItem.ContentType);
                buildingContentItem.VersionRecord = buildingItemVersionRecord;

                var context = new VersionContentContext
                {
                    Id = existingContentItem.Id,
                    ContentType = existingContentItem.ContentType,
                    ContentItemRecord = contentItemRecord,
                    ExistingContentItem = existingContentItem,
                    BuildingContentItem = buildingContentItem,
                    ExistingItemVersionRecord = existingItemVersionRecord,
                    BuildingItemVersionRecord = buildingItemVersionRecord,
                };
                Handlers.Invoke(handler => handler.Versioning(context), Logger);
                Handlers.Invoke(handler => handler.Versioned(context), Logger);

                yield return context.BuildingContentItem;
            }
        }

        /// <summary>
        /// Gets a draft version of the provided item.
        /// </summary>
        /// <param name="item">Item to build draft for.</param>
        /// <returns>Draft version.</returns>
        public ContentItem GetDraft(ContentItem item) {
            return GetDraft(new[] {item}).FirstOrDefault();
        }

        public IEnumerable<ContentItem> GetDraft(params ContentItem[] items)
        {
            var builtDrafts = FindDraftsFor(
                items.Where(item => {
                        ContentItem draft;
                        return !_cache.TryGetValue(item.Id, out draft) || !draft.As<VersionInfoPart>().Draft;
                     })
                     .Select(i => i.Id)
                     .ToArray())
                .ToDictionary(i => i.Id);

            var existingDrafts = items.Select(item => {
                ContentItem draft;
                if (!_cache.TryGetValue(item.Id, out draft) || !draft.As<VersionInfoPart>().Draft) return new { Item = item, Draft = builtDrafts.ContainsKey(item.Id) ? builtDrafts[item.Id] : null };
                return new { Item = item, Draft = draft };
            }).ToList();

            var itemsToBuildNewVersionFrom = existingDrafts.Where(i => i.Draft == null).Select(tuple => tuple.Item).ToArray();
            var newVersions = NewVersion(itemsToBuildNewVersionFrom);

            var finalCollection = existingDrafts.Where(i => i.Draft != null).Select(i => i.Draft).Concat(newVersions).ToArray();
            EnsureDrafts(finalCollection);

            foreach (var draft in finalCollection)
            {
                if (draft != null)
                {
                    _cache[draft.Id] = draft;
                }
                yield return draft;
            }
        }

        /// <summary>
        /// Clears any existing draft of a provided item, setting it back to it's published state.
        /// </summary>
        /// <param name="item">Item to clear draft for.</param>
        public void ClearDraft(ContentItem item)
        {
            ClearDraft(new[]{ item });
        }

        public void ClearDraft(params ContentItem[] items)
        {
            var ids = items.Select(r => r.Id).ToArray();
            var latestVersions = _contentItemVersionRepository.Table
                .Where(r => ids.Contains(r.ContentItemRecord.Id) && r.Latest)
                .OrderByDescending(r => r.Id)
                .ToDictionary(r => r.ContentItemRecord.Id);

            var publishedVersions = _contentItemVersionRepository.Table
                .Where(r => ids.Contains(r.ContentItemRecord.Id) && r.Published)
                .OrderByDescending(r => r.Id)
                .ToDictionary(r => r.ContentItemRecord.Id);


            var currentDrafts = FindDraftsFor(ids);

            foreach (var item in items)
            {
                if (latestVersions.ContainsKey(item.Id) && !latestVersions[item.Id].Published)
                {
                    latestVersions[item.Id].Latest = false;
                    if (publishedVersions.ContainsKey(item.Id))
                    {
                        publishedVersions[item.Id].Latest = true;
                    }
                }
            }

            // We have to ensure that the only draft (if exists) has been unmarked as draft
            foreach(var item in currentDrafts)
            {
                item.As<VersionInfoPart>().Draft = false;
            }
        }

        /// <summary>
        /// Looks up an existing draft for an item with given id.
        /// </summary>
        /// <param name="ids"></param>
        /// <returns>Existing draft or null, if none found.</returns>
        protected IEnumerable<ContentItem> FindDraftsFor(params int[] ids) {
            // There can be only one Draft-marked item for a given content item
            var drafts = GetQueryable()
                .Where(x => x.ContentItemVersionRecord != null && ids.Contains(x.ContentItemRecord.Id) && x.Draft)
                .OrderBy(x => x.Id)
                .ToLookup(x => x.ContentItemRecord.Id);

            foreach(var entry in drafts)
            {
                ContentItem retVal = null;
                var draft = entry.SingleOrDefault();
                try
                {
                    retVal = draft != null ? _content.Get(-1, VersionOptions.VersionRecord(draft.ContentItemVersionRecord.Id)) : null;
                }
                catch (Exception ex)
                {
                    // If we went here, something really bad happened (db is corrupted?).
                    // Marking the draft found as false, so it won't appear in the subsequent queries.
                    Logger.Error(ex, "Error when searching for draft for menu item {0}", entry.Key);
                    if (draft == null) continue;

                    draft.Draft = false;
                    _infoPartRepository.Update(draft);
                    _infoPartRepository.Flush();
                }

                yield return retVal;
            }
        }

        /// <summary>
        /// Gets the current, published version of a given item.
        /// </summary>
        /// <param name="item">Item to get the published version for.</param>
        /// <returns>Published version of an item.</returns>
        public IContent GetCurrent(IContent item)
        {
            return item.ContentItem.VersionRecord.Published 
                ? item 
                : _content.Get(item.Id, VersionOptions.Published);
        }

        /// <summary>
        /// Gets the version with
        /// </summary>
        /// <param name="id"></param>
        /// <param name="versionNumber"></param>
        /// <returns></returns>
        public ContentItem GetVersion(int id, int versionNumber) {
            return _content.Get(id, VersionOptions.Number(versionNumber));
        }

        /// <summary>
        /// Checks if an item has a removed version.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool HasRemovedVersion(int id)
        {
            return GetQueryable().Any(r => r.Removed && r.ContentItemRecord.Id == id);
        }

        /// <summary>
        /// Checks if an item has a draft version.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool HasDraftVersion(int id)
        {
            return GetQueryable().Any(r => r.Draft && r.ContentItemRecord.Id == id);
        }

        /// <summary>
        /// Checks if an item has a published version.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool HasPublishedVersion(int id)
        {
            return GetQueryable().Any(r => r.ContentItemVersionRecord.Published && r.ContentItemRecord.Id == id);
        }

        /// <summary>
        /// Checks if an item has a latest version.
        /// </summary>
        /// <param name="id">Id of an item.</param>
        /// <returns></returns>
        public bool HasLatestVersion(int id)
        {
            return GetQueryable().Any(r => r.ContentItemVersionRecord.Latest && r.ContentItemRecord.Id == id);
        }

        /// <summary>
        /// Publishes a given version of an item.
        /// </summary>
        /// <param name="item">Item to publish</param>
        public void PublishVersion(ContentItem item) {
            Publish(item);

            if (item.Is<VersionInfoPart>()) {
                item.As<VersionInfoPart>().Removed = false;
                item.As<VersionInfoPart>().Draft = false;
            }
        }

        public void PublishVersions(params ContentItem[] items)
        {
            Publish(items);

            foreach (var item in items.Where(item => item.Is<VersionInfoPart>()))
            {
                item.As<VersionInfoPart>().Removed = false;
                item.As<VersionInfoPart>().Draft = false;
            }
        }

        /// <summary>
        /// Performs the actual publish of a given item.
        /// </summary>
        /// <param name="contentItems">Item to be published.</param>
        private void Publish(params ContentItem[] contentItems)
        {
            var itemsToProcess = contentItems.Where(i => !i.VersionRecord.Published).ToList();
            var ids = itemsToProcess.Select(i => i.Id).ToArray();

            // create a context for the item and its previous published record
            var previousVersions = _contentItemVersionRepository.Table
                .Where(r => ids.Contains(r.ContentItemRecord.Id) && r.Published)
                .OrderByDescending(r => r.Id)
                .ToLookup(r => r.ContentItemRecord.Id);

            foreach (var contentItem in itemsToProcess)
            {
                var previous = previousVersions[contentItem.Id].ToList();
                var context = new PublishContentContext(contentItem, previous.FirstOrDefault());

                // invoke handlers to acquire state, or at least establish lazy loading callbacks
                Handlers.Invoke(handler => handler.Publishing(context), Logger);

                if (context.Cancel)
                {
                    return;
                }

                foreach(var p in previous)
                {
                    p.Published = false;
                }

                contentItem.VersionRecord.Published = true;

                Handlers.Invoke(handler => handler.Published(context), Logger);
            }
        }

        /// <summary>
        /// Removes a given version of the item.
        /// </summary>
        /// <param name="item">Item to remove.</param>
        public void RemoveVersion(ContentItem item) {
            _content.Remove(item);

            if (item.Is<VersionInfoPart>()) {
                item.As<VersionInfoPart>().Removed = true;
                item.As<VersionInfoPart>().Draft = false;
            }
        }

        public void RemoveVersions(params ContentItem[] items)
        {
            foreach(var item in items)
                RemoveVersion(item);
        }

        /// <summary>
        /// Sets given item version as latest.
        /// </summary>
        /// <param name="item">Item to set latest on.</param>
        public void SetLatest(ContentItem item) {
            var latestVersion = _contentItemVersionRepository.Table
                .Where(r => r.ContentItemRecord == item.Record)
                .SingleOrDefault(r => r.Latest);

            if (latestVersion != null) {
                latestVersion.Latest = false;
            }

            item.VersionRecord.Latest = true;
        }

        /// <summary>
        /// Returns the underlying queryable object for quering item versions.
        /// </summary>
        /// <returns></returns>
        public IQueryable<VersionInfoPartRecord> GetQueryable()
        {
            return _infoPartRepository.Table;
        }

        /// <summary>
        /// Ensures that the given items is correctly marked as draft.
        /// </summary>
        /// <param name="items"></param>
        private void EnsureDrafts(params ContentItem[] items)
        {
            var ids = items.Select(item => item.Id).ToArray();
            var draftVersions = GetQueryable().Where(i => ids.Contains(i.ContentItemRecord.Id) && i.Draft)
                .ToLookup(r => r.ContentItemVersionRecord.ContentItemRecord.Id);

            foreach (var item in items) {
                if (draftVersions.Contains(item.Record.Id))
                {
                    foreach (var draftVersion in draftVersions[item.Record.Id])
                    {
                        draftVersion.Draft = false;
                    }
                }

                item.As<VersionInfoPart>().Draft = true;
            }
        }
    }
}
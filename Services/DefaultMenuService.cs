﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Web.Hosting;
using NHibernate.SqlCommand;
using Onestop.Navigation.Models;
using Onestop.Navigation.Scheduling;
using Onestop.Navigation.Utilities;
using Orchard;
using Orchard.Autoroute.Models;
using Orchard.Caching.Services;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Records;
using Orchard.ContentPicker.Models;
using Orchard.Core.Common.Models;
using Orchard.Core.Navigation.Models;
using Orchard.Core.Title.Models;
using Orchard.Data;
using Orchard.Environment;
using Orchard.Logging;
using Orchard.Services;
using Orchard.Tasks.Scheduling;
using Orchard.UI;
using Orchard.UI.Notify;

namespace Onestop.Navigation.Services {
    public class DefaultMenuService : Component, IMenuService {
        public const string MenuItemsCacheSignal = "Onestop.Navigation.MenuItems";

        private readonly IClock _clock;
        private readonly IContentManager _contentManager;
        private readonly ICacheService _cache;
        private readonly IPublishMenuTaskManager _tasks;
        private readonly IVersionManager _versions;
        private readonly IOrchardServices _services;
        private readonly IRepository<ContentItemVersionRecord> _contentItemVersionRepository;
        private readonly IRepository<ExtendedMenuItemPartRecord> _itemRepository;
        private readonly INotifier _notifier;
        private readonly ISingletonLock _singletonLock;

        // Request-scoped caching
        private IEnumerable<IContent> _menus;
        private ILookup<int, ContentItemVersionRecord> _menuVersionLookup; 

        public DefaultMenuService(
            IClock clock,
            IContentManager contentManager,
            ICacheService cache,
            IPublishMenuTaskManager tasks,
            IVersionManager versions,
            IOrchardServices services,
            IRepository<ContentItemVersionRecord> contentItemVersionRepository,
            IRepository<ExtendedMenuItemPartRecord> itemRepository,
            INotifier notifier,
            ISingletonLock singletonLock) {
            _clock = clock;
            _contentManager = contentManager;
            _cache = cache;
            _tasks = tasks;
            _versions = versions;
            _services = services;
            _contentItemVersionRepository = contentItemVersionRepository;
            _itemRepository = itemRepository;
            _notifier = notifier;
            _singletonLock = singletonLock;
        }

        /// <summary>
        /// Returns a list of menu item versions scheduled to be published.
        /// </summary>
        /// <param name="menuId">Id of a menu content item.</param>
        /// <returns></returns>
        public IEnumerable<IScheduledTask> GetScheduledMenuVersions(int menuId) {
            return _tasks.GetMenuPublishTasks(menuId);
        }

        /// <summary>
        /// Deletes a menu with a given id.
        /// </summary>
        /// <param name="menuId"></param>
        protected void DeleteMenu(int menuId) {
            var item = GetMenu(menuId);
            if (item == null || item.ContentItem == null) {
                return;
            }

            // Remove menu items
            foreach (var i in GetMenuItems(item, VersionOptions.Latest)) {
                _contentManager.Remove(i.ContentItem);
            }

            // Remove the menu
            _contentManager.Remove(item.ContentItem);
        }

        /// <summary>
        /// Marks the item to delete when menu gets published.
        /// </summary>
        /// <param name="itemId">
        /// The item id.
        /// </param>
        /// <param name="versionId">Version of the item to delete.</param>
        public void DeleteMenuItem(int itemId, int versionId = 0) {
            var item = versionId > 0
                ? GetMenuItem(-1, VersionOptions.VersionRecord(versionId))
                : GetMenuItem(itemId, VersionOptions.Latest);

            if (item == null || item.ContentItem == null) {
                throw new ArgumentException(T("Item with ID {0} cannot be found.", itemId).Text);
            }

            Expression<Func<ExtendedMenuItemPartRecord, bool>> predicate =
                i =>
                    i.Position != null && i.Position != "0" &&
                    i.Position.StartsWith(item.As<ExtendedMenuItemPart>().Position + ".");
            var children = versionId > 0
                ? GetMenuItems(item.As<MenuPart>().Menu,
                    VersionOptions.VersionRecord(item.As<ExtendedMenuItemPart>().Menu.ContentItem.VersionRecord.Id),
                    predicate)
                : GetMenuItems(item.As<MenuPart>().Menu, VersionOptions.Latest, predicate).ToList();

            foreach (var i in children.Concat(new[] {item})) {
                var menuItem = i.As<ExtendedMenuItemPart>();

                // If item is a newly added item, put it back in the "unassociated" list
                if (menuItem.IsNew && menuItem.IsCurrent) {
                    UnpublishMenuItemInternal(menuItem);
                    continue;
                }

                // If item is new, unassociated item, remove it completely
                if (menuItem.IsNew) {
                    _versions.RemoveVersion(i.ContentItem);
                    continue;
                }

                // If item has been changed, clear the draft.
                if (menuItem.IsChanged) {
                    ClearDraft(i.Id);
                    continue;
                }

                // If item is neither new nor changed, create a new version and mark it for removal
                var draft = versionId > 0 ? i : GetMenuItem(i.Id, VersionOptions.DraftRequired);
                draft.As<VersionInfoPart>().Removed = true;
            }
        }

        /// <summary>
        /// Restores a given removed menu item version.
        /// </summary>
        /// <param name="versionId">Id of a version to restore</param>
        /// <param name="newVersion">Should create a new draft version instead of simply undeleting a given version?</param>
        public void UndeleteMenuItem(int versionId, bool newVersion) {
            var item = GetMenuItem(-1, VersionOptions.VersionRecord(versionId));

            if (item == null || item.ContentItem == null) {
                return;
            }

            if (newVersion) {
                // if we're trying to undelete some version, we should create a new version
                // for that we need to temporarily mark fetched version as latest
                item.ContentItem.VersionRecord.Latest = true;
                item = GetMenuItem(item.Id, VersionOptions.DraftRequired);
                item.As<ExtendedMenuItemPart>().Position = null;
                item.As<ExtendedMenuItemPart>().MenuVersion = null;
            }

            item.As<VersionInfoPart>().Removed = false;
        }

        /// <summary>
        /// Retrieves a menu by its name.
        /// </summary>
        /// <param name="menuName">The menu name.</param>
        /// <param name="options"></param>
        /// <returns>A menu content item or null if not found.</returns>
        public IContent GetMenu(string menuName, VersionOptions options = null) {
            var item = _contentManager.Query<TitlePart, TitlePartRecord>()
                .Where(x => x.Title == menuName)
                .ForType("Menu")
                .Slice(0, 1)
                .FirstOrDefault();

            if (item != null) {
                if (options == null) {
                    return item;
                }

                return _contentManager.Get(item.Id, options);
            }

            return null;
        }

        /// <summary>
        /// Retrieves a menu by its id.
        /// </summary>
        /// <param name="menuId">The menu content item id.</param>
        /// <param name="options"></param>
        /// <returns>A menu content item or null if not found.</returns>
        public IContent GetMenu(int menuId, VersionOptions options = null) {
            return _contentManager.Get(menuId, options ?? VersionOptions.Published);
        }

        /// <summary>
        /// Gets the given menu change history.
        /// </summary>
        /// <param name="menuId">The menu content item id.</param>
        /// <returns>List of menu versions</returns>
        public IEnumerable<IContent> GetMenuHistory(int menuId) {
            return _contentManager.GetAllVersions(menuId)
                .OrderBy(item => item.Version)
                .ToList();
        }

        /// <summary>
        /// Lists all removed menu items.
        /// </summary>
        /// <param name="menuId">The menu content item id.</param>
        /// <param name="menuVersionNumber">(Optional) Version of the menu to get items for.</param>
        /// <returns>List of removed menu items.</returns>
        public IEnumerable<IContent> GetRemovedMenuItems(int menuId, int menuVersionNumber = 0) {
            if (menuVersionNumber > 0) {
                // Get items marked as removed in that version
                var versionRecord =
                    _contentItemVersionRepository.Get(
                        r => r.ContentItemRecord.Id == menuId && r.Number == menuVersionNumber);

                return _contentManager
                    .Query<ExtendedMenuItemPart, ExtendedMenuItemPartRecord>(VersionOptions.AllVersions)
                    .WithQueryHintsForMenuItem()
                    .Where(i => i.MenuVersionRecord.Id == versionRecord.Id)
                    .Join<MenuPartRecord>()
                    .Where(m => m.MenuId == menuId)
                    .Join<VersionInfoPartRecord>()
                    .Where(v => v.Removed)
                    .List();
            }

            // Get items not existing at all in a given menu (neither latest nor published)
            var versions = _contentManager
                .Query<ExtendedMenuItemPart, ExtendedMenuItemPartRecord>(VersionOptions.AllVersions)
                .WithQueryHintsForMenuItem()
                .Join<MenuPartRecord>()
                .Where(m => m.MenuId == menuId)
                .Join<VersionInfoPartRecord>()
                .Where(v => v.Removed)
                .List()
                .Where(i => !i.ContentItem.Record.Versions.Any(v => v.Published || v.Latest))
                .GroupBy(item => item.Id)
                .Select(g => g.OrderBy(item => item.ContentItem.Version).Last());

            return versions;
        }

        /// <summary>
        /// The get menu item.
        /// </summary>
        /// <param name="itemId">
        /// The item id.
        /// </param>
        /// <param name="options">Optional version options.</param>
        /// <param name="baseVersionIdForDraft">Optional base version to build draft on. Used only in conjunction with VersionOptions.DraftRequired passed as options.</param>
        /// <returns>
        /// Menu item.
        /// </returns>
        public IContent GetMenuItem(int itemId, VersionOptions options = null, int baseVersionIdForDraft = default(int)) {
            if (options != null && options.IsDraftRequired) {
                // Build draft if necessary, based on latest/specific version
                var item = baseVersionIdForDraft != default(int)
                    ? _contentManager.Get(itemId, VersionOptions.VersionRecord(baseVersionIdForDraft))
                    : _contentManager.Get(itemId, VersionOptions.Latest);

                if (item != null) {
                    item = _versions.GetDraft(item);
                    item.As<ExtendedMenuItemPart>().MenuVersion = null;
                    return item;
                }

                return null;
            }

            return _contentManager.Get(itemId, options ?? VersionOptions.Latest).As<ExtendedMenuItemPart>();
        }

        /// <summary>
        /// Creates a menu item with a given content type.
        /// </summary>
        /// <param name="menuId">Id of a menu to bind created item to.</param>
        /// <param name="type">Content type to create.</param>
        /// <returns>New menu item.</returns>
        public IContent CreateMenuItem(int menuId, string type) {
            var item = _contentManager.New(type);

            if (item.Is<ExtendedMenuItemPart>()) {
                var menu = GetMenu(menuId);

                item.As<ExtendedMenuItemPart>().Position = null;
                item.As<ExtendedMenuItemPart>().DisplayHref = true;
                item.As<ExtendedMenuItemPart>().DisplayText = true;
                item.As<ExtendedMenuItemPart>().MenuVersion = null;
                item.As<MenuPart>().Menu = menu;
            }
            return item;
        }

        /// <summary>
        /// Gets the existing menus.
        /// </summary>
        /// <returns>
        /// Collection of menus.
        /// </returns>
        public IEnumerable<IContent> GetMenus() {
            if (_menus == null) {
                try {
                    WaitUntilPublished();
                    _menus = _contentManager
                        .Query<TitlePart, TitlePartRecord>()
                        .Where(x => x.Title != null)
                        .ForType("Menu")
                        .List();
                }
                catch (Exception) {
                    return Enumerable.Empty<MenuPart>();
                }
            }
            return _menus;
        }

        /// <summary>
        /// Retrieves all menu items for specified menu.
        /// </summary>
        /// <param name="menu">The menu to get items for.</param>
        /// <param name="options">Optional version options.</param>
        /// <param name="enforceConsistency">Should enforce consistency by unpublishing invalid items?</param>
        /// <param name="predicate">Optional predicate for item filtering on database level.</param>
        /// <param name="preloadProperties">Should menu item properties be preloaded ahead of time?</param>
        /// <returns>List of published menu items or, if 'options' are specified, list of items for a specific menu version.</returns>
        public IEnumerable<IContent> GetMenuItems(
            IContent menu,
            VersionOptions options = null,
            Expression<Func<ExtendedMenuItemPartRecord, bool>> predicate = null,
            bool enforceConsistency = true,
            bool preloadProperties = true) {
            options = options ?? VersionOptions.Published;

            WaitUntilPublished();

            // Building the final query
            var results = GetMenuItemsQuery(menu, options, predicate)
                .List()
                .ToList();

            // Preloading item properties
            if (preloadProperties) {
                PreloadItemsProperties(results);
            }

            // Keeping published menus in consistent state in case something went wrong during publishing.
            // Note: This should happen in the handler, but that ends up with a SOE.
            if (enforceConsistency) {
                bool inconsitencyFound = false;
                foreach (var version in results.Where(v => !CheckConsistency(v))) {
                    inconsitencyFound = true;
                    version.Position = null;
                    version.MenuVersion = null;
                    _versions.RemoveVersion(version.ContentItem);
                    Logger.Warning("Found and removed a corrupted menu item '{0}'.", version.Id);
                }

                if (inconsitencyFound) {
                    _contentManager.Clear();

                    // If we encountered inconsistency, start from the beginning
                    return GetMenuItems(menu, options, predicate, false, preloadProperties);
                }
            }

            // Version number/id based options return all versions, so those must be grouped together
            return options.IsLatest || options.IsPublished
                ? results
                : results.GroupBy(i => i.ContentItem.Id,
                    (i, group) => @group.OrderByDescending(item => item.ContentItem.VersionRecord.Id).FirstOrDefault());
        }

        /// <summary>
        /// Counts menu items that match given criteria.
        /// </summary>
        /// <param name="menu"></param>
        /// <param name="options"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public int GetMenuItemsCount(IContent menu, VersionOptions options = null,
            Expression<Func<ExtendedMenuItemPartRecord, bool>> predicate = null) {
            return GetMenuItemsQuery(menu, options, predicate).Count();
        }

        /// <summary>
        /// Creates a lookup from menu ids to version records.
        /// </summary>
        /// <returns>A lookup from menu ids to version records</returns>
        public ILookup<int, ContentItemVersionRecord> GetMenuVersionLookup() {
            var menus = GetMenus().ToList();

            var menuIds = menus.Select(m => m.Id).ToArray();

            if (_menuVersionLookup == null || _menuVersionLookup.Any(l => !l.Any()))
            {
                var versions = _contentItemVersionRepository
                    .Table
                    .Where(v => v.ContentItemRecord != null && menuIds.Contains(v.ContentItemRecord.Id));

                _menuVersionLookup = versions.ToLookup(v => v.ContentItemRecord.Id);
            }
            return _menuVersionLookup;
        }

        /// <summary>
        /// Builds a query for menu items for further processing.
        /// </summary>
        /// <param name="menu"></param>
        /// <param name="options"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IContentQuery<ExtendedMenuItemPart> GetMenuItemsQuery(IContent menu, VersionOptions options = null,
            Expression<Func<ExtendedMenuItemPartRecord, bool>> predicate = null) {
            int versionRecordId = default(int);
            options = options ?? VersionOptions.Published;

            // Initial query
            var query = _contentManager
                .Query<ExtendedMenuItemPart, ExtendedMenuItemPartRecord>(VersionOptions.AllVersions)
                .Where(i => i.Position != null && i.Position != "0");

            if (options.IsPublished) {
                //versionRecordId = _contentItemVersionRepository.Get(r => r.ContentItemRecord.Id == menu.Id && r.Published).Id;
                query = _contentManager
                    .Query<ExtendedMenuItemPart, ExtendedMenuItemPartRecord>(VersionOptions.Published)
                    .Where(i => i.Position != null && i.Position != "0");
            }
            else if (options.IsLatest) {
                query = _contentManager.Query<ExtendedMenuItemPart, ExtendedMenuItemPartRecord>(VersionOptions.Latest);
            }

            if (options.VersionNumber != default(int) || options.VersionRecordId != default(int)) {
                // Seek for the appropriate version id and include in query
                if (options.VersionNumber != default(int))
                    versionRecordId =
                        _contentItemVersionRepository.Get(
                            r => r.ContentItemRecord.Id == menu.Id && r.Number == options.VersionNumber).Id;
                else if (options.VersionRecordId != default(int))
                    versionRecordId = options.VersionRecordId;

                if (versionRecordId != default(int)) {
                    query = query.Where(i => i.MenuVersionRecord.Id == versionRecordId);
                }
                else {
                    throw new ArgumentException("Provided version record cannot be found", "options");
                }
            }

            if (predicate != null) {
                query = query.Where(predicate);
            }

            // Building the final query
            return query
                .WithQueryHintsForMenuItem()
                .Join<VersionInfoPartRecord>()
                .Join<CommonPartRecord>()
                .Join<MenuPartRecord>()
                .Where(m => m.MenuId == menu.Id);
        }

        /// <summary>
        /// Preloads certain properties of each item on a list ahead of time, for performance.
        /// </summary>
        /// <param name="items">List of menu items to preload data for.</param>
        /// <param name="options"></param>
        private void PreloadItemsProperties(List<ExtendedMenuItemPart> items, VersionOptions options = null) {
            options = options ?? VersionOptions.Published;

            var itemIds = items.Select(i => i.Id)
                .Distinct()
                .ToArray();

            // Preloading item properties ahead of time.
            var changedVersions = _versions.FilterDrafts(itemIds).ToDictionary(id => id);
            var publishedVersions = _versions.FilterPublished(itemIds).ToDictionary(id => id);
            var latestVersions = _versions.FilterLatest(itemIds).ToDictionary(id => id);
            var allPublished = _contentManager
                .Query<ExtendedMenuItemPart>(options)
                .Where<ExtendedMenuItemPartRecord>(r => itemIds.Contains(r.ContentItemRecord.Id))
                .List()
                .ToLookup(item => item.Id);

            foreach (var item in items) {
                item.HasPublishedField.Value = publishedVersions.ContainsKey(item.Id);
                item.HasLatestField.Value = latestVersions.ContainsKey(item.Id);
                item.IsChangedField.Value = changedVersions.ContainsKey(item.Id);

                // Preloading published versions
                if (!item.ContentItem.VersionRecord.Published && allPublished.Contains(item.Id)) {
                    var published = allPublished[item.Id].OrderByDescending(i => i.ContentItem.VersionRecord.Id).First();
                    // Checking number of versions, in case something got corrupted on database level
                    if (allPublished[item.Id].Count() > 1) {
                        var message =
                            T(
                                "Multiple published versions found for item {0}: {1}. Chosen the most recent one: {2}. Please re-publish the menu.",
                                item.Id,
                                string.Join(", ", allPublished[item.Id].Select(i => i.ContentItem.VersionRecord.Id)),
                                published.ContentItem.VersionRecord.Id);

                        _notifier.Error(message);
                        Logger.Error(message.Text);
                    }
                    item.PublishedVersionField.Value = published;
                }
                else {
                    item.PublishedVersionField.Value = item;
                }
            }

            // Preloading content items referenced by ContentMenuItemPart
            var itemsToPreload = items
                .Where(i => i.Is<ContentMenuItemPart>())
                .Select(i => i.As<ContentMenuItemPart>().Record.ContentMenuItemRecord)
                .Where(r => r != null)
                .Select(r => r.Id)
                .Distinct()
                .ToArray();

            // ReSharper disable once UnusedVariable
            var preloaded = _contentManager.Query()
                .Where<CommonPartRecord>(r => itemsToPreload.Contains(r.ContentItemRecord.Id))
                .Join<AutoroutePartRecord>()
                .Join<TitlePartRecord>()
                .List();
        }

        /// <summary>
        /// Checks if a given menu item is in consistent, correct state.
        /// </summary>
        /// <param name="item">Item to check</param>
        /// <returns>True if ok, false if item data is corrupted.</returns>
        private bool CheckConsistency(ExtendedMenuItemPart item) {
            // If the position is there, the item has to be either current, removed or a draft
            if (item.HasPosition && !item.IsCurrent && !item.IsRemoved && !item.IsDraft)
                return false;

            // Item cannot be published without having a latest version
            if (!item.HasLatest && item.IsPublished)
                return false;

            return true;
        }

        /// <summary>
        /// Publishes a given menu.
        /// </summary>
        /// <param name="menuId">Id of the menu content item.</param>
        /// <param name="versionId">Optional version identifier of the menu version to be published.</param>
        public void PublishMenu(int menuId, int versionId = 0) {
            lock (_singletonLock) {
                try
                {
                    SignalPublishStart();
                    _services.TransactionManager.RequireNew(IsolationLevel.ReadCommitted);

                    var menu = versionId > 0
                        ? GetMenu(menuId, VersionOptions.VersionRecord(versionId))
                        : CreateNewVersion(menuId);

                    // If specific version is requested then retrieve only those items.
                    // If no version specified, then get all latest items
                    // Filter out those with no position set
                    Expression<Func<ExtendedMenuItemPartRecord, bool>> predicate =
                        i => i.Position != null && i.Position != "0";

                    // We don't have to check for consistency
                    var items =
                        GetMenuItems(menu,
                            versionId > 0 ? VersionOptions.VersionRecord(versionId) : VersionOptions.Latest, predicate,
                            false).ToList();

                    _versions.PublishVersion(menu.ContentItem);
                    _versions.RemoveVersions(
                        items.Where(i => i.As<ExtendedMenuItemPart>().IsRemoved).Select(i => i.ContentItem).ToArray());
                    _versions.PublishVersions(
                        items.Where(i => !i.As<ExtendedMenuItemPart>().IsRemoved).Select(i => i.ContentItem).ToArray());

                    ClearCache(menuId);
                    _tasks.DeleteTasks(menu.ContentItem, task => task.ContentItem.Version == menu.ContentItem.Version);

                    // Forcing the transaction to commit/rollback
                    _services.TransactionManager.RequireNew();

                }
                catch
                {
                    _services.TransactionManager.Cancel();
                    throw;
                }
                finally {
                    try {
                        SignalPublishEnd();
                    }
                    catch (Exception ex) {
                        Logger.Error(ex, "Error when signalling publication end.");
                    }
                }
            }
        }

        /// <summary>
        /// Sets up a scheduled publishing task for a given menu.
        /// </summary>
        /// <param name="menuId">Menu content item id.</param>
        /// <param name="scheduledPublishUtc">Date and time to publish the menu.</param>
        /// <param name="baseMenuVersion">Base menu version to build the new one from.</param>
        public void SchedulePublication(int menuId, DateTime scheduledPublishUtc, int? baseMenuVersion = null) {
            var version = CreateNewVersion(menuId, baseMenuVersion);
            _tasks.SchedulePublication(version.ContentItem, scheduledPublishUtc);
        }

        /// <summary>
        /// Schedules a publication of a given menu version at a given date and time.
        /// </summary>
        /// <param name="version">Version of a menu to be published.</param>
        /// <param name="scheduledPublishUtc">Publication date.</param>
        public void SchedulePublication(IContent version, DateTime scheduledPublishUtc) {
            _tasks.SchedulePublication(version.ContentItem, scheduledPublishUtc);
        }

        /// <summary>
        /// Cancels previously scheduled menu version publication.
        /// </summary>
        /// <param name="version">Version of a menu to cancel publication of.</param>
        public void CancelSchedule(IContent version) {
            _tasks.DeleteTasks(version.ContentItem, task => version.ContentItem.Version == task.ContentItem.Version);
            var allMenuVersions = GetMenuHistory(version.Id);
            var scheduledVersions = GetScheduledMenuVersions(version.Id);

            // If it's scheduled then it's ok
            // If it's not scheduled and not published yet (ie. "cancelled"), then it's not ok
            var versionToSetAsLatest = allMenuVersions
                .OrderByDescending(item => item.ContentItem.Version)
                .FirstOrDefault(item =>
                    item.ContentItem.VersionRecord.Id != version.ContentItem.VersionRecord.Id &&
                    (scheduledVersions.Any(
                        task => task.ContentItem.VersionRecord.Id == item.ContentItem.VersionRecord.Id) ||
                     item.As<CommonPart>().VersionPublishedUtc.HasValue));

            if (version.ContentItem.VersionRecord.Latest) {
                version.ContentItem.VersionRecord.Latest = false;
                var versionItems = GetMenuItems(version, VersionOptions.VersionRecord(version.ContentItem.Version));
                foreach (var item in versionItems) {
                    item.ContentItem.VersionRecord.Latest = false;
                }

                if (versionToSetAsLatest != null) {
                    _versions.SetLatest(versionToSetAsLatest.ContentItem);
                    var versionToSetAsLatestItems = GetMenuItems(version,
                        VersionOptions.VersionRecord(versionToSetAsLatest.ContentItem.Version));
                    foreach (var item in versionToSetAsLatestItems) {
                        // If there is a draft (item is changed) or item has been removed - do not set the latest flag
                        if (!item.As<ExtendedMenuItemPart>().IsChanged) {
                            if (item.As<ExtendedMenuItemPart>().IsRemoved) {
                                item.ContentItem.VersionRecord.Latest = false;
                                item.ContentItem.VersionRecord.Published = false;
                            }
                            else {
                                item.ContentItem.VersionRecord.Latest = true;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Creates a new version of the menu.
        /// </summary>
        /// <param name="menuId">Identifier of the menu.</param>
        /// <param name="versionNumber">Optional version number to base new version on.</param>
        /// <returns>Number of the version created.</returns>
        public IContent CreateNewVersion(int menuId, int? versionNumber = null) {
            WaitUntilPublished();

            lock (_singletonLock) {
                var menu = GetMenu(menuId);

                if (menu == null) {
                    throw new ArgumentException("Specified menu does not exist", "menuId");
                }

                try {
                    // Creating new, unpublished, not-latest version
                    var newVersion = _versions.NewVersion(menu.ContentItem);

                    // Get the list of latest (draft or published) versions of menu items
                    // TODO: This should be done more performant by using a predicate parameter for database-level filtering
                    // A simple predicate will help to limit the number of items fetched
                    Expression<Func<ExtendedMenuItemPartRecord, bool>> predicate;

                    if (versionNumber == null)
                        predicate =
                            i =>
                                i.Position != null && i.Position != "0" &&
                                i.ContentItemRecord.Versions.Any(v => v.Published);
                    else predicate = i => i.Position != null && i.Position != "0";

                    var items = versionNumber == null
                        ? GetMenuItems(menu, VersionOptions.Latest, predicate)
                        : GetMenuItems(menu, VersionOptions.Number(versionNumber.Value), predicate);

                    var itemsToPublish =
                        items.Where(i => i.As<ExtendedMenuItemPart>().IsCurrent)
                            .OrderBy(i => i.As<ExtendedMenuItemPart>().Position, new FlatPositionComparer());
                    foreach (var draft in _versions.GetDraft(itemsToPublish.Select(i => i.ContentItem).ToArray())) {
                        SetUpItemForVersion(draft.As<ExtendedMenuItemPart>(), newVersion);
                    }

                    _services.TransactionManager.RequireNew();
                    return newVersion;
                }
                catch {
                    _services.TransactionManager.Cancel();
                    throw;
                }
            }
        }

        /// <summary>
        /// Deletes a given version of the menu. Not implemented.
        /// </summary>
        /// <param name="menuId">Ide of the menu content item.</param>
        /// <param name="versionId">Number of the version to delete.</param>
        public void DeleteVersion(int menuId, int versionId) {
            throw new NotImplementedException();
        }

        private void SetUpItemForVersion(ExtendedMenuItemPart item, IContent menuVersion) {
            item.MenuVersion = menuVersion;
            item.As<VersionInfoPart>().Draft = false;
        }

        /// <summary>
        /// Unpublishes a given menu item, making it a draft.
        /// </summary>
        /// <param name="menuId">Menu content item id.</param>
        /// <param name="itemId">Item.</param>
        /// <param name="options"></param>
        public void UnpublishMenuItem(int menuId, int itemId, VersionOptions options = null) {
            WaitUntilPublished();

            var menu = GetMenu(menuId);
            var parent = GetMenuItem(itemId).As<ExtendedMenuItemPart>();

            options = options ?? VersionOptions.Latest;

            // Get child items
            Expression<Func<ExtendedMenuItemPartRecord, bool>> predicate =
                i =>
                    i.Position != null && i.Position != "0" &&
                    i.Position.StartsWith(parent.As<ExtendedMenuItemPart>().Position + ".");
            var childItems = GetMenuItems(menu, options, predicate, false);

            foreach (var item in childItems.Concat(new[] {parent})) {
                UnpublishMenuItemInternal(item.As<ExtendedMenuItemPart>());
            }
        }

        private void UnpublishMenuItemInternal(ExtendedMenuItemPart item) {
            // If item is new item, put it back in the "unassociated" list
            _contentManager.Unpublish(item.ContentItem);
            item.Position = null;
            item.MenuVersion = null;
            item.As<VersionInfoPart>().Draft = true;
        }

        /// <summary>
        /// Computes new positions for given items.
        /// </summary>
        /// <param name="parentId">Parent item id.</param>
        /// <param name="newChildren">List of child items.</param>
        /// <param name="menuId"></param>
        public void UpdatePositionsFor(int menuId, int parentId, IEnumerable<int> newChildren) {
            WaitUntilPublished();

            lock (_singletonLock) {
                _services.TransactionManager.RequireNew(IsolationLevel.ReadCommitted);
                var menu = GetMenu(menuId);
                if (menu == null) {
                    Logger.Error(T("Menu with ID {0} does not exist.", menuId).Text);
                    return;
                }

                var parent =
                    _itemRepository.Table.FirstOrDefault(
                        r => r.ContentItemRecord.Id == parentId && r.ContentItemVersionRecord.Latest);

                var parentPosition = parentId > 0 && parent != null
                    ? parent.Position + "."
                    : string.Empty;

                var changedItems =
                    _itemRepository.Table.Where(
                        r => r.ContentItemVersionRecord.Latest && newChildren.Contains(r.ContentItemRecord.Id))
                        .ToDictionary(r => r.ContentItemRecord.Id);

                var changedVersionInfos = _versions.GetQueryable().Where(
                    r => r.ContentItemVersionRecord.Latest && newChildren.Contains(r.ContentItemRecord.Id))
                    .ToDictionary(r => r.ContentItemRecord.Id);

                var rootItemsToBuildDraftsFor = new Dictionary<int, ChangedItem>();


                int i = 1;

                foreach (var id in newChildren) {
                    if (!changedItems.ContainsKey(id)) {
                        continue;
                    }

                    var latest = changedItems[id];
                    var oldPosition = latest.Position ?? "";
                    var newPosition = parentPosition + i;

                    i++;

                    if (oldPosition != newPosition && changedVersionInfos.ContainsKey(id) &&
                        !changedVersionInfos[id].Removed) {
                        rootItemsToBuildDraftsFor[id] = new ChangedItem {
                            Id = id,
                            VersionId = latest.Id,
                            OldPosition = oldPosition,
                            NewPosition = newPosition
                        };
                    }
                }

                var allItemsToBuildDraftsFor = new Dictionary<int, ChangedItem>(rootItemsToBuildDraftsFor);

                // fetching children of the changed items
                foreach (var kvp in rootItemsToBuildDraftsFor) {
                    var parentPosLength = kvp.Value.OldPosition.Length;

                    // if item was unassociated, continue
                    if (string.IsNullOrWhiteSpace(kvp.Value.OldPosition)) continue;

                    var changedChildItems =
                        GetMenuItems(menu, VersionOptions.Latest,
                            r => r.Position.StartsWith(kvp.Value.OldPosition + '.'));

                    var childIds = changedChildItems.Select(x => x.Id).ToList();

                    var changedChildVersionInfos = _versions.GetQueryable()
                        .Where(r => r.ContentItemVersionRecord.Latest && childIds.Contains(r.ContentItemRecord.Id))
                        .ToDictionary(r => r.ContentItemRecord.Id);

                    foreach (var child in changedChildItems) {
                        if (changedChildVersionInfos.ContainsKey(child.Id) &&
                            !changedChildVersionInfos[child.Id].Removed &&
                            !allItemsToBuildDraftsFor.ContainsKey(child.Id)) {
                            allItemsToBuildDraftsFor[child.Id] = new ChangedItem {
                                Id = child.Id,
                                VersionId = child.ContentItem.VersionRecord.Id,
                                OldPosition = child.As<ExtendedMenuItemPart>().Position,
                                NewPosition =
                                    kvp.Value.NewPosition + '.' +
                                    child.As<ExtendedMenuItemPart>().Position.Substring(parentPosLength).TrimStart('.')
                            };
                        }
                    }
                }

                var draftIds = allItemsToBuildDraftsFor.Keys.ToArray();

                // Fetching all drafts in one shot
                var itemsToDraft =
                    _contentManager.Query<ExtendedMenuItemPart, ExtendedMenuItemPartRecord>(VersionOptions.Latest)
                        .Where(r => draftIds.Contains(r.ContentItemRecord.Id))
                        .WithQueryHintsForMenuItem()
                        .List()
                        .Select(r => r.ContentItem).ToArray();

                var newItems = _versions.GetDraft(itemsToDraft);

                // Updating all positions
                foreach (var item in newItems) {
                    item.As<ExtendedMenuItemPart>().Position = allItemsToBuildDraftsFor[item.Id].NewPosition;
                    if (item.Is<VersionInfoPart>()) {
                        item.As<VersionInfoPart>().Author = _services.WorkContext.CurrentUser;
                    }

                    if (item.Is<CommonPart>()) {
                        item.As<CommonPart>().VersionModifiedUtc = _clock.UtcNow;
                    }
                }

                // Forcing the transaction to commit/rollback
                _services.TransactionManager.RequireNew();
            }
        }

        /// <summary>
        /// Computes new positions for given items.
        /// </summary>
        /// <param name="parentVersionId">Parent item id.</param>
        /// <param name="newChildrenVersions">List of child items.</param>
        /// <param name="menuId"></param>
        public void UpdatePositionsForPreview(int menuId, int parentVersionId, IEnumerable<int> newChildrenVersions) {
            WaitUntilPublished();

            var menu = GetMenu(menuId);

            var parent = GetMenuItem(-1, VersionOptions.VersionRecord(parentVersionId)).As<ExtendedMenuItemPart>();
            var parentPosition = parentVersionId > 0 && parent != null
                ? parent.Position + "."
                : string.Empty;

            // Changing new hierarchy
            int i = 1;
            var newItems = newChildrenVersions
                .Select(child => GetMenuItem(-1, VersionOptions.VersionRecord(child)))
                .Where(mItem => mItem != null && !mItem.As<ExtendedMenuItemPart>().IsRemoved)
                .ToList();

            Expression<Func<ExtendedMenuItemPartRecord, bool>> predicate =
                child => child.Position != null && child.Position != "0";
            var items = GetMenuItems(menu, predicate: predicate).ToList();
            var newItemsChildren = newItems.ToDictionary(
                k => k.Id,
                v => items
                    .Where(
                        child =>
                            child.As<ExtendedMenuItemPart>()
                                .Position.StartsWith((v.As<ExtendedMenuItemPart>().Position ?? "") + "."))
                    .Select(child => GetMenuItem(-1, VersionOptions.VersionRecord(child.ContentItem.VersionRecord.Id)))
                    .Where(mItem => mItem != null && !mItem.As<ExtendedMenuItemPart>().IsRemoved)
                    .ToList());

            foreach (var item in newItems) {
                var oldPosition = item.As<ExtendedMenuItemPart>().Position ?? "";
                var newPosition = parentPosition + i;

                foreach (var child in newItemsChildren[item.Id]) {
                    child.As<ExtendedMenuItemPart>().Position = newPosition +
                                                                child.As<ExtendedMenuItemPart>()
                                                                    .Position.Remove(0, oldPosition.Length);
                }

                item.As<ExtendedMenuItemPart>().Position = newPosition;
                i++;
            }
        }

        /// <summary>
        /// Clears draft (if any) of a given menu item.
        /// </summary>
        /// <param name="id">Menu item id.</param>
        public void ClearDraft(int id) {
            var item = _contentManager.Get(id, VersionOptions.Latest)
                       ?? _contentManager.Get(id, VersionOptions.Published);

            if (item != null && item.Is<ExtendedMenuItemPart>()) {
                try {
                    _versions.ClearDraft(item);
                }
// ReSharper disable once EmptyGeneralCatchClause
                catch {
                    // Do nothing - if it fails it means that there is no draft (record nonexistent)
                }
            }
        }

        /// <summary>
        /// Clears all existing drafts for a given menu.
        /// </summary>
        /// <param name="menuId">Menu content item id.</param>
        public void ClearAllDrafts(int menuId) {
            WaitUntilPublished();

            var menu = GetMenu(menuId);
            var items = GetMenuItems(menu, VersionOptions.Latest)
                .Where(i => !i.As<ExtendedMenuItemPart>().IsDraft && i.As<ExtendedMenuItemPart>().HasDraft())
                .Select(i => i.ContentItem);

            var splitted = items.ToLookup(x => x.As<ExtendedMenuItemPart>().IsNew);

            _versions.ClearDraft(splitted[false].ToArray());

            foreach (var item in splitted[true]) {
                // If item is new item, put it back in the "unassociated" list
                UnpublishMenuItemInternal(item.As<ExtendedMenuItemPart>());
            }
        }

        /// <summary>
        /// Sets an arbitrary position string as a position for a given item.
        /// </summary>
        /// <param name="menuId">The menu content item id.</param>
        /// <param name="itemId">The item id.</param>
        /// <param name="newPosition">The new position.</param>
        /// <returns>
        /// </returns>
        public bool SetPosition(int menuId, int itemId, string newPosition) {
            WaitUntilPublished();

            lock (_singletonLock) {
                var item = GetMenuItem(itemId);
                if (!item.As<ExtendedMenuItemPart>().IsNew || item.As<ExtendedMenuItemPart>().Menu.Id != menuId) {
                    return false;
                }

                if (!newPosition.Trim()
                    .Split('.')
                    .All(s => {
                        int i;
                        return int.TryParse(s.Trim(), out i) && i > 0;
                    })) {
                    return false;
                }

                try {
                    var menu = GetMenu(menuId);
                    var lastDot = newPosition.LastIndexOf('.');
                    var parentPosition = lastDot != -1 ? newPosition.Substring(0, lastDot) : "";
                    var parentItem = string.IsNullOrWhiteSpace(parentPosition)
                        ? null
                        : GetMenuItems(menu, VersionOptions.Latest,
                            rec => rec.Position != null && rec.Position.Equals(parentPosition)).First();
                    var parentLevel = newPosition.Count(c => c == '.');

                    item.As<ExtendedMenuItemPart>().Position = newPosition;

                    // Get the conflicting item(s)
                    var siblings = GetMenuItems(menu, VersionOptions.Latest,
                        rec => rec.Position != null && rec.Position != ""
                               && rec.Position.StartsWith(parentPosition) && rec.ContentItemRecord.Id != item.ContentItem.Id);
                    var siblingIds = new[] {item.As<ExtendedMenuItemPart>()}
                        .Concat(siblings
                            .Select(s => s.As<ExtendedMenuItemPart>())
                            .Where(i => !i.IsRemoved && i.IsCurrent)
                            .Where(i => i.Position.Count(c => c == '.') == parentLevel))
                        .OrderBy(i => i.Position, new FlatPositionComparer())
                        .Select(i => i.Id);

                    UpdatePositionsFor(menuId, parentItem != null ? parentItem.Id : default(int), siblingIds.ToList());

                    return true;
                }
                catch (Exception ex) {
                    Logger.Error(ex, T("Cannot set menu item {0} position to {1}.", item.Id, newPosition).Text);
                    _services.TransactionManager.Cancel();
                    return false;
                }
                finally {
                    // Forcing the transaction to commit
                    _services.TransactionManager.RequireNew();
                }
            }
        }

        /// <summary>
        /// Clears all caches for a given menu.
        /// </summary>
        /// <param name="menuId">Menu content item id.</param>
        public void ClearCache(int menuId) {
            _cache.SignalChange("Menu." + menuId);
        }

        public void WaitUntilPublished() {
            // Active wait is bad, but we have no better way unless we explicitly use some pub/sub server like Redis.
#pragma warning disable 219
            object value;
#pragma warning restore 219
            while ((value = _cache.Get("Navigation.PublishInProgress")) != null && value.ToString() != CurrentThreadKey()) {
                Thread.Sleep(1000);
            }
        }

        public void SignalPublishStart() {
            _cache.Put("Navigation.PublishInProgress", CurrentThreadKey());
        }

        public void SignalPublishEnd() {
            _cache.Remove("Navigation.PublishInProgress");
        }

        private string CurrentThreadKey() {
            return Environment.MachineName + "::" + Thread.CurrentThread.ManagedThreadId;
        }
    }

    internal class ChangedItem {
        public int Id { get; set; }
        public int VersionId { get; set; }
        public string OldPosition { get; set; }
        public string NewPosition { get; set; }
    }
}
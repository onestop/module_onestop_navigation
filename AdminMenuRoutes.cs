﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Orchard.Environment.Extensions;
using Orchard.Mvc.Routes;

namespace Onestop.Navigation {
    [OrchardFeature("Onestop.Navigation.AdminMenu")]
    public class AdminMenuRoutes : IRouteProvider {
        public void GetRoutes(ICollection<RouteDescriptor> routes) {
            foreach (var routeDescriptor in GetRoutes()) {
                routes.Add(routeDescriptor);
            }
        }

        public IEnumerable<RouteDescriptor> GetRoutes() {
            return new[] {
                    new RouteDescriptor {
                            Priority = 150, 
                            Route =
                                new Route(
                                "Admin/Onestop/Navigation/AdminMenu", 
                                new RouteValueDictionary {
                                        { "area", "Onestop.Navigation" }, 
                                        { "controller", "AdminNavigationAdmin" }, 
                                        { "action", "Index" },
                                }, 
                                new RouteValueDictionary(), 
                                new RouteValueDictionary { { "area", "Onestop.Navigation" } }, 
                                new MvcRouteHandler())
                    }
                };
        }
    }
}
﻿namespace Onestop.Navigation.Breadcrumbs.Services
{
    public class BreadcrumbsProviderDescriptor
    {
        public string Name { get; set; }
        public string DisplayText { get; set; }
    }
}
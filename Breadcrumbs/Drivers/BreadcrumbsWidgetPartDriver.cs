﻿using System.Web;
using Onestop.Navigation.Services;
using Onestop.Navigation.Breadcrumbs.Models;
using Onestop.Navigation.Breadcrumbs.Services;
using Orchard.Caching.Services;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.Core.Common.Utilities;
using Orchard.Environment.Extensions;
using Orchard.Localization;
using Orchard.Logging;
using Orchard.Mvc;
using Orchard.Widgets.Models;

namespace Onestop.Navigation.Breadcrumbs.Drivers
{
    [OrchardFeature("Onestop.Navigation.Breadcrumbs")]
    public class BreadcrumbsWidgetPartDriver : ContentPartDriver<BreadcrumbsWidgetPart>
    {
        private readonly IBreadcrumbsService _breadcrumbs;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ICacheService _cache;

        public Localizer T { get; set; }
        public ILogger Logger { get; set; }

        public BreadcrumbsWidgetPartDriver(
            IBreadcrumbsService breadcrumbs,
            IHttpContextAccessor httpContextAccessor,
            ICacheService cache)
        {
            _breadcrumbs = breadcrumbs;
            _httpContextAccessor = httpContextAccessor;
            _cache = cache;
            T = NullLocalizer.Instance;
            Logger = NullLogger.Instance;
        }

        protected override DriverResult Display(BreadcrumbsWidgetPart part, string displayType, dynamic shapeHelper)
        {
            return ContentShape("Parts_Menu_BreadcrumbsWidget",
                () =>
                {
                    var currentContext = _httpContextAccessor.Current();
                    var shapeKey = string.Format("Shape::BreadcrumbsWidget::{0}_{1}__{2}",
                        part.As<WidgetPart>().Zone,
                        part.As<WidgetPart>().LayerPart.Name,
                        currentContext.Request.Path.Trim('/', ' ').ToLowerInvariant());

                    // Fetching the cached widget output
                    bool requireRefresh;
                    int version;
                    var shapeVal = _cache.Get(shapeKey, GetDifferentiator(), out requireRefresh, out version);
                    dynamic breadcrumbsShape = new HtmlString(shapeVal);

                    var widgetShape = shapeHelper.Parts_Menu_BreadcrumbsWidget(ContentItem: part.ContentItem,
                        ContentPart: part);

                    // If refresh is required - re-render the whole breadcrumbs.
                    if (requireRefresh)
                    {
                        var lazyBreadcrumbs = new LazyField<Services.Breadcrumbs>();
                        lazyBreadcrumbs.Loader(() => _breadcrumbs.Build());
                        breadcrumbsShape = shapeHelper.Breadcrumbs(
                            LazyBreadcrumbs: lazyBreadcrumbs,
                            ContentItem: part.ContentItem,
                            Separator: "\\",
                            PleaseCacheKey: shapeKey,
                            PleaseCacheVersion: version);
                    }

                    return widgetShape.Add(breadcrumbsShape);
                });
        }

        private static string GetDifferentiator()
        {
            return "Breadcrumbs";
        }
    }
}
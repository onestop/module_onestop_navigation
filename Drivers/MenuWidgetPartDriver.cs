﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using Onestop.Navigation.Models;
using Onestop.Navigation.Services;
using Onestop.Navigation.Utilities;
using Onestop.Navigation.ViewModels;
using Orchard;
using Orchard.Caching.Services;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Aspects;
using Orchard.ContentManagement.Drivers;
using Orchard.Data;
using Orchard.DisplayManagement;
using Orchard.Localization;
using Orchard.Logging;
using Orchard.Mvc;
using Orchard.UI;
using Orchard.UI.Admin;
using Orchard.UI.Navigation;
using Orchard.UI.Notify;
using Orchard.Utility.Extensions;
using Orchard.Widgets.Models;
using Newtonsoft.Json;

namespace Onestop.Navigation.Drivers {
    public class MenuWidgetPartDriver : ContentPartDriver<OnestopMenuWidgetPart> {
        private const string TemplateName = "Parts/Menu.Widget.Edit";

        private readonly IContentManager _contentManager;
        private readonly IWorkContextAccessor _workContextAccessor;
        private readonly ICacheService _cache;
        private readonly dynamic _shapeFactory;
        private readonly ITransactionManager _transactions;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMenuService _menuService;
        private readonly INavigationManager _nav;
        private readonly INotifier _notifier;

        public MenuWidgetPartDriver(
            IMenuService menuService,
            INotifier notifier,
            IHttpContextAccessor httpContextAccessor,
            INavigationManager nav,
            IContentManager contentManager, 
            IWorkContextAccessor workContextAccessor, 
            ICacheService cache, 
            IShapeFactory shapeFactory, 
            ITransactionManager transactions)
        {
            _menuService = menuService;
            _notifier = notifier;
            _httpContextAccessor = httpContextAccessor;
            _nav = nav;
            _contentManager = contentManager;
            _workContextAccessor = workContextAccessor;
            _cache = cache;
            _shapeFactory = shapeFactory;
            _transactions = transactions;
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }
        public ILogger Logger { get; set; }

        protected override DriverResult Display(OnestopMenuWidgetPart part, string displayType, dynamic shapeHelper) {
            // Do not try to render the menu widget when in Dashboard
            if (AdminFilter.IsApplied(_workContextAccessor.GetContext().HttpContext.Request.RequestContext)) return null;

            switch (displayType) {
                case "Detail":
                    return ContentShape("Parts_Menu_Widget", () => BuildDisplayShape(part));
                default:
                    return null;
            }
        }

        protected override DriverResult Editor(OnestopMenuWidgetPart part, dynamic shapeHelper) {
            var model = new MenuWidgetViewModel {
                CurrentMenuId = part.Menu == null ? -1 : part.Menu.Id,
                CutOrFlattenLower = part.CutOrFlattenLower,
                Levels = part.Levels,
                Mode = part.Mode,
                RootNode = part.RootNode,
                WrapChildrenInDivs = part.WrapChildrenInDiv,
                UseCache = part.UseCache,
                Menus = _menuService.GetMenus(),
                Modes = Enum.GetValues(typeof(MenuWidgetMode)).Cast<MenuWidgetMode>()
            };

            return ContentShape("Parts_Menu_Widget", () => shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: model, Prefix: Prefix));
        }

        protected override DriverResult Editor(OnestopMenuWidgetPart part, IUpdateModel updater, dynamic shapeHelper) {
            var model = new MenuWidgetViewModel();

            if (updater.TryUpdateModel(model, Prefix, null, null)) {

                // Clearing old menu cache
                if (part.Menu != null && model.UseCache) {
                    _cache.SignalChange(GetDifferentiator(part.Menu.Id));
                }

                part.CutOrFlattenLower = model.CutOrFlattenLower;
                part.Levels = model.Levels;
                part.Mode = model.Mode;
                part.RootNode = model.RootNode;
                part.WrapChildrenInDiv = model.WrapChildrenInDivs;
                part.Menu = _contentManager.Get(model.CurrentMenuId).Record;
                part.UseCache = model.UseCache;

                // Clearing new menu cache
                if (part.Menu != null && model.UseCache) {
                    _cache.SignalChange(GetDifferentiator(part.Menu.Id));
                }

                _notifier.Information(T("Menu widget edited successfully"));
            }
            else
            {
                _notifier.Error(T("Error during menu widget update!"));
            }

            return Editor(part, shapeHelper);
        }

        /// <summary>
        /// Builds driver display shape.
        /// </summary>
        /// <param name="part">Menu widget part.</param>
        /// <returns></returns>
        private dynamic BuildDisplayShape(OnestopMenuWidgetPart part)
        {
            if (part.Menu == null)
            {
                return null;
            }

            var request = _httpContextAccessor.Current().Request;
            var requestData = request.RequestContext.RouteData;
            var requestDictionary = requestData.Values.Where(val => val.Value != null).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

            // Adding query string parameters to dictionary
            foreach (string key in request.QueryString.Keys)
            {
                if (key == null) continue;
                if (!requestDictionary.ContainsKey(key) && request.QueryString[key] != null)
                {
                    requestDictionary[key] = request.QueryString[key];
                }
            }

            var isCalledByName = requestDictionary.ContainsKey("menuItemName");
            var currentContext = _httpContextAccessor.Current();
            var currentCulture = _workContextAccessor.GetContext().CurrentCulture;

            // Render widget only for MVC routes
            var extension = Path.GetExtension(currentContext.Request.Path);
            if (!string.IsNullOrWhiteSpace(extension))
                return null;

            // Fetching the cached widget output
            var requireRefresh = !part.UseCache;
            dynamic menuShape = new HtmlString("");
            var version = default(int);
            string shapeKey = null;

            if (part.UseCache)
            {
                shapeKey = string.Format("Shape::OnestopMenuWidget::{8}_{0}_{1}_{2}_{3}_{4}_{5}_{6}_{7}___{9}",
                    part.Menu.Id,
                    part.Mode,
                    part.Levels,
                    part.RootNode,
                    part.WrapChildrenInDiv,
                    part.CutOrFlattenLower,
                    isCalledByName ? requestDictionary["menuItemName"] : "",
                    currentCulture,
                    part.As<WidgetPart>().Zone,
                    currentContext.Request.Path.Trim('/', ' ').ToLowerInvariant());
                var menuShapeVal = _cache.Get(shapeKey, GetDifferentiator(part.Menu.Id), out requireRefresh, out version);
                menuShape = new HtmlString(menuShapeVal);
            }

            // If refresh is required - re-render the whole menu.
            if (requireRefresh)
            {
                // Forcing read committed mode to not have dirty reads when publishing/menu editing is in progress
                _transactions.RequireNew(IsolationLevel.ReadCommitted);

                var menu = _menuService.GetMenu(part.Menu.Id);

                if (menu == null)
                {
                    return null;
                }

                Stack<MenuItem> selectedPath;
                var itemsDictionary = GetItemsDictionary(part, requestDictionary, out selectedPath);
                var selectedPathJson = selectedPath == null ? null : JsonConvert.SerializeObject(selectedPath.Select(item => new {name = item.Text == null ? null : item.Text.Text, href = item.Href}));

                menuShape = _shapeFactory.Menu()
                    .Menu(menu)
                    .MenuName(menu.As<ITitleAspect>().Title.HtmlClassify())
                    .ZoneName(part.As<WidgetPart>().Zone.HtmlClassify())
                    .ItemId(part.ContentItem.Id.ToString(CultureInfo.InvariantCulture))
                    .SelectedPath(selectedPathJson)
                    .PleaseCacheKey(shapeKey)
                    .PleaseCacheVersion(version);

                var shapes = itemsDictionary.Select(pair =>
                {
                    var level = pair.Key.Split('.', ',').Length;
                    return new KeyValuePair<string, dynamic>(
                        pair.Key, BuildMenuItemShape(
                            null,
                            menuShape,
                            pair.Value,
                            part,
                            level));
                }).ToDictionary(kv => kv.Key, kv => kv.Value);

            var topLevel = shapes.Any() ? shapes.Values.Min(s => (int) s.Level()) : 0;

                // Set up parent-child relationships
                // We have to do it after populating the whole shape dictionary.
                foreach (var pair in shapes.OrderBy(kv => kv.Key, new FlatPositionComparer()))
                {
                    var splitted = pair.Key.Split('.', ',');

                    dynamic parent;
                    if (shapes.TryGetValue(string.Join(".", splitted.Take(splitted.Length - 1)), out parent))
                    {
                        pair.Value.Parent(parent);
                        // Due to ordering, parents will always come before it's children
                        // so they already have their level corrected.
                        pair.Value.Level(parent.Level + 1);
                        parent.Add(pair.Value, pair.Key);
                    }
                    else if (splitted.Length == topLevel)
                    {
                        // Due to ordering, parents will always come before it's children
                        pair.Value.Level(1);
                        pair.Value.Parent(menuShape);
                        menuShape.Add(pair.Value, pair.Key);
                    }
                    else
                    {
                        var content = ((IContent)pair.Value.Content).As<ExtendedMenuItemPart>();
                        // We're dealing with an orphaned item - log this information
                        Logger.Debug(T("An orphaned menu item was found - {0}. Database may need to be cleaned up.", content).Text);
                    }
                }
            }

            // Forcing the transaction to commit/rollback
            _transactions.RequireNew();

            var partShape = _shapeFactory.Parts_Menu_Widget().Add(menuShape);
            return partShape;

        }

        /// <summary>
        /// Builds a shape object for a given menu item.
        /// </summary>
        /// <param name="parentShape">Parent shape</param>
        /// <param name="menu">Menu shape</param>
        /// <param name="menuItem">Menu item object</param>
        /// <param name="settings">Widget settings</param>
        /// <param name="level">Menu item level</param>
        /// <returns></returns>
        protected dynamic BuildMenuItemShape(dynamic parentShape, dynamic menu, MenuItem menuItem, OnestopMenuWidgetPart settings, int level)
        {
            var item = _shapeFactory.MenuItem()
                .Text(menuItem.Text)
                .IdHint(menuItem.IdHint)
                .Href(menuItem.Href)
                .LinkToFirstChild(menuItem.LinkToFirstChild)
                .LocalNav(menuItem.LocalNav)
                .Selected(menuItem.Selected)
                .RouteValues(menuItem.RouteValues)
                .Item(menuItem)
                .Menu(menu)
                .Parent(parentShape)
                .Content(menuItem.Content)
                .WrapChildrenInDiv(settings.WrapChildrenInDiv)
                                    .Level(level)
                                    .ZoneName(menu.ZoneName);

            if (menuItem.Content != null && menuItem.Content.Is<ExtendedMenuItemPart>())
            {
                var part = menuItem.Content.As<ExtendedMenuItemPart>();

                item.DisplayHref(part.DisplayHref)
                    .DisplayText(part.DisplayText)
                    .SubTitle(part.SubTitle)
                    .Group(part.GroupName)
                    .InNewWindow(part.InNewWindow);

                if (!string.IsNullOrWhiteSpace(part.Classes))
                {
                    foreach (var c in part.Classes.Split(new[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        item.Classes.Add(c.Trim());
                    }
                }

                if (!string.IsNullOrWhiteSpace(part.CssId))
                {
                    item.Id = part.CssId.Trim();
                }
            }

            var currentContext = _httpContextAccessor.Current();

            /* Setting currently selected item */
            item.Current(currentContext != null &&
                (UrlUtility.RouteMatches(menuItem.RouteValues, currentContext.Request.RequestContext.RouteData.Values) ||
                UrlUtility.UrlMatches(menuItem.Href, currentContext.Request.Path, currentContext)));

            return item;
        }

        /// <summary>
        /// Flattens the provided hierarchical/nested collection of menu items and populates a provided dictionary with the resulting data. 
        /// Dictionary is keyed with item position.
        /// </summary>
        /// <param name="items">Hierarchy of menu items, as fetched from navigation manager.</param>
        /// <param name="currentDict">Dictionary to fill.</param>
        /// <param name="positionFilter">Delegate that filters initial list of items based on position.</param>
        private static void PopulateDictionary(IEnumerable<MenuItem> items, IDictionary<string, MenuItem> currentDict, Func<string, bool> positionFilter)
        {
            if (items == null) return;

            foreach (var item in items)
            {
                if (positionFilter != null && !positionFilter(item.Position)) return;

                currentDict[item.Position] = item;
                PopulateDictionary(item.Items, currentDict, positionFilter);
            }
        }

        /// <summary>
        /// Builds a dictionary of items for a given menu widget.
        /// </summary>
        /// <param name="part">Menu widget for which to fetch the dictionary.</param>
        /// <param name="requestDictionary">Combined request data and query string parameters.</param>
        /// <param name="selectedPath">The selected path</param>
        /// <returns></returns>
        private IDictionary<string, MenuItem> GetItemsDictionary(OnestopMenuWidgetPart part, IDictionary<string, object> requestDictionary, out Stack<MenuItem> selectedPath) {
            var request = _httpContextAccessor.Current().Request;
            var requestData = request.RequestContext.RouteData;
            var isCalledByName = requestDictionary.ContainsKey("menuItemName");
            var menuItems = _nav.BuildMenu(_contentManager.Get(part.Menu.Id));
            var currentCulture = _workContextAccessor.GetContext().CurrentCulture;

            var localized = new List<MenuItem>();
            foreach (var menuItem in menuItems) {
                // if there is no associated content, treat it as culture neutral
                if (menuItem.Content == null) {
                    localized.Add(menuItem);
                }

                // if the menu item is culture neutral or of the current culture
                else if (String.IsNullOrEmpty(menuItem.Culture) || String.Equals(menuItem.Culture, currentCulture, StringComparison.OrdinalIgnoreCase)) {
                    localized.Add(menuItem);
                }
            }

            menuItems = localized;

            // Set the currently selected path
            selectedPath = MenuItemsUtility.SetSelectedPath(
                menuItems,
                requestData,
                _httpContextAccessor.Current().Request.Path,
                _httpContextAccessor.Current());

            if (!string.IsNullOrWhiteSpace(part.RootNode)) {
                var item = MenuItemsUtility.GetItemByPosition(menuItems, part.RootNode);
                if (item != null) {
                    menuItems = item.Items.OrderBy(m => m.Position, new FlatPositionComparer()).ToList();
                }
            }

            var matchedItem = isCalledByName ? MenuItemsUtility.GetItemByName(menuItems, requestDictionary["menuItemName"].ToString())
                                             : MenuItemsUtility.GetItemByUrl(menuItems, requestData, request.Path, _httpContextAccessor.Current());

            switch (part.Mode) {
                case MenuWidgetMode.AllItems:
                    break;
                case MenuWidgetMode.ChildrenOnly:
                    menuItems = matchedItem != null 
                        ? matchedItem.Items
                                     .OrderBy(m => m.Position, new FlatPositionComparer())
                                     .ToList()
                        : Enumerable.Empty<MenuItem>();
                    break;
                case MenuWidgetMode.SiblingsOnly:

                    var parent = MenuItemsUtility.GetParent(menuItems, matchedItem);
                    if (parent != null) {
                        menuItems = parent.Items.OrderBy(m => m.Position, new FlatPositionComparer()).ToList();
                    }

                    break;
                case MenuWidgetMode.SiblingsExpanded:
                    if(!MenuItemsUtility.ClearChildren(menuItems, new[]{ matchedItem }))
                    {
                        Logger.Warning("Matching menu item not found in SiblingsExpanded mode. All items are collapsed.");
                    }
                    break;
            }

            // Unfolding a built menu to a flat item dictionary
            var dict = new Dictionary<string, MenuItem>();
            PopulateDictionary(menuItems, dict, position => {
                                                var level = position.Split('.', ',').Length;
                                                return !(part.Levels > 0 && level > part.Levels);
                                            });

            return dict;
        }

        /// <summary>
        /// Returns a differentiator string for a given menu id.
        /// </summary>
        /// <param name="menuId">Menu id.</param>
        /// <returns>Differentiator string.</returns>
        private static string GetDifferentiator(int menuId) {
            return "Menu." + menuId;
        }
    }
}